# Barde

[CppCms](http://cppcms.com) web-based application.
Compose and share playlists with your friends.


# Build and execute

The web application can be run through docker containers.

The `run.sh` script helps to build and excute.

## Prerequisite

The following tools are required:
* g++
* make
* cmake
* sassc
* docker
* docker-compose

The following libraries are required:
* libcppcms 2.0.0.beta2 (from https://sourceforge.net/projects/cppcms/files/cppcms/)
* libcppdb 0.3.1
* libpcre 2:8.39-15
* libicu 72.1-3
* libcrypt 1:4.4.33-2
* libz 4.8.12-3.1

## Execute

From project root directory:
```sh
./run.sh
```

## Rebuild

`run.sh` rebuilds the fast CGI container only, to rebuild all:

From `docker` directory:
```sh
docker-compose stop
docker-compose build
```

## Deployment

* upload docker/ and www/folders
* change the `change_xxx` and `changeme` passwords in docker folders (in docker-compose conf and sql scrips)
* create /var/www/barde soft link that points to www/public/ folder
* install docker-compose and sassc 
* from www/public/css, execute `sassc main.scss playlist.css`
* finally, from docker/ folder, execute `docker-compose up -d`

The web-application should be up and listening on port 80.
Log in with the users accounts defined in table users in docker/db/02-barde-data.sql.
