
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';


USE `barde`;


CREATE TABLE IF NOT EXISTS `artist` (
  `id` UUID DEFAULT UUID(),
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


CREATE TABLE IF NOT EXISTS `media` (
  `id` UUID DEFAULT UUID(),
  `type` enum('avatar','playlist','song') NOT NULL,
  `label` varchar(20) NOT NULL,
  `file` varchar(100) NOT NULL,
  `content_type` varchar(20) NOT NULL,
  `length` int(9) unsigned NOT NULL,
  `md5sum` varchar(100) DEFAULT NULL,
  `creator_id` UUID NOT NULL,
  `enabled` tinyint(1) unsigned zerofill NOT NULL,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  CONSTRAINT `creator_id` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


CREATE TABLE IF NOT EXISTS `playlist` (
  `id` UUID DEFAULT UUID(),
  `name` varchar(50) NOT NULL,
  `image_id` UUID DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `player` enum('simple','jwplayer') NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  KEY `image_id` (`image_id`),
  CONSTRAINT `playlist_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


CREATE TABLE IF NOT EXISTS `playlist_comment` (
  `id` UUID DEFAULT UUID(),
  `playlist_id` UUID NOT NULL,
  `parent_id` UUID DEFAULT NULL,
  `comment` text NOT NULL,
  `author_id` UUID DEFAULT NULL,
  `status` enum('published','signaled','blocked','approved') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  KEY `playlist_id` (`playlist_id`),
  KEY `parent_id` (`parent_id`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `playlist_comment_ibfk_1` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`),
  CONSTRAINT `playlist_comment_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `playlist_comment` (`id`),
  CONSTRAINT `playlist_comment_ibfk_3` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


CREATE TABLE IF NOT EXISTS `playlist_vote` (
  `playlist_id` UUID NOT NULL,
  `user_id` UUID NOT NULL,
  `vote` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`playlist_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `playlist_vote_ibfk_1` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`),
  CONSTRAINT `playlist_vote_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


CREATE TABLE IF NOT EXISTS `song` (
  `id` UUID DEFAULT UUID(),
  `title` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `artist_id` UUID NOT NULL,
  `file_id` UUID DEFAULT NULL,
  `url` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `duration` smallint(4) unsigned DEFAULT NULL,
  `playlist_id` UUID,
  `position` smallint(4) unsigned NOT NULL,
  `proposer_id` UUID NOT NULL,
  `show_video` tinyint(1) DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  KEY `proposer_id` (`proposer_id`),
  KEY `artist_id` (`artist_id`),
  KEY `file_id` (`file_id`),
  CONSTRAINT `song_ibfk_1` FOREIGN KEY (`proposer_id`) REFERENCES `user` (`id`),
  CONSTRAINT `song_ibfk_2` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`),
  CONSTRAINT `song_ibfk_3` FOREIGN KEY (`file_id`) REFERENCES `media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


CREATE TABLE IF NOT EXISTS `song_vote` (
  `song_id` UUID NOT NULL,
  `user_id` UUID NOT NULL,
  `vote` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`song_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `song_vote_ibfk_1` FOREIGN KEY (`song_id`) REFERENCES `song` (`id`),
  CONSTRAINT `song_vote_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


CREATE TABLE IF NOT EXISTS `user` (
  `id` UUID DEFAULT UUID(),
  `alias` varchar(20) NOT NULL,
  `avatar_id` UUID DEFAULT NULL,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `level` smallint(3) unsigned NOT NULL DEFAULT 10,
  `privacy` enum('private','public') NOT NULL DEFAULT 'private',
  `skip_disliked` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `alias` (`alias`),
  UNIQUE KEY `avatar_id` (`avatar_id`),
  CONSTRAINT `avatar` FOREIGN KEY (`avatar_id`) REFERENCES `media` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
