#!/bin/sh -e


# pre-requisites:
# make, cmake, g++, sassc, docker, docker-compose


BUILD_DIR=build
WWW_DIR=/var/www/barde

echo "Build fast CGI executable ..."
mkdir -p $BUILD_DIR
cd $BUILD_DIR
cmake ../src
make
cd -
pwd
ls -lrt $BUILD_DIR
cp $BUILD_DIR/barde docker/fcgi/bin/barde
pwd
ls -lrt docker/fcgi/bin
echo ".. done"
echo

echo "Prepare and copy static files ..."
mkdir -p $WWW_DIR
chmod u+rwx $WWW_DIR
cp -R www/public/* $WWW_DIR
cd $WWW_DIR/css
sassc main.scss playlist.css
cd -
echo ".. done"
echo


echo "Build and start docker services ..."
cd docker
docker-compose stop fcgi
docker-compose build fcgi
docker-compose up -d
docker-compose ps
echo ".. done"
