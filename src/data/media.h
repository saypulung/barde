#ifndef DATA_MEDIA_H
#define DATA_MEDIA_H

#include <cppcms/http_file.h>
#include <cppcms/serialization.h>
#include <cppcms/view.h>

#include <string>
#include <sstream>


namespace data {

    struct Media : public cppcms::base_content, cppcms::serializable {
        static constexpr const char* TYPE_AVATAR = "avatar";
        static constexpr const char* TYPE_PLAYLIST = "playlist";
        static constexpr const char* TYPE_SONG = "song";

        std::string id;
        std::string type;
        std::string label;
        std::string file;
        std::string contentType;
        unsigned long length;
        std::string md5sum;
        time_t createdAt;

        Media() {
            clear();
        }

        Media(std::string type, std::string label, std::string filePath, cppcms::http::file* file) :
            id(""), type(type), label(label), file(filePath), contentType(file->mime()),
            length(file->size())
        {}

        void clear() {
            id = "";
            label = "";
            file = "";
        }

        void serialize(cppcms::archive &a) {
            a & id & type & label & file;
        }

        std::string toString() const {
            std::ostringstream oss;
            oss << "{ id: " << id << ", type: " << type << ", label: " << label
                << ", file: " << file << ", contentType: " << contentType
                << ", length: " << length << " }";
            return oss.str();
        }
    };

}   // namespace data


#endif  // DATA_MEDIA_H;
