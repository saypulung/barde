#ifndef SONG_MAPPER_H
#define SONG_MAPPER_H


#include "dbMapper.h"
#include "song.h"
#include <data/pages/song.h>


namespace data {

    class SongMapper : public DbMapper {
    public:
        static const std::string SQL_GLOBAL_VOTES;

        SongMapper(const std::string& connectionString);

        bool loadSong(Song& dest, const std::string& songId);
        bool loadUserProposedSongs(User& dest);
        bool loadPendingSongs(SongAdminPage& dest);

        bool insert(const User& proposer, const Song& song);
        bool update(const Song& song);
        bool updatePlaylistId(const std::string& songId, const std::string& playlistId);
        bool updateDuration(const std::string& songId, unsigned int duration);
    };


}   // namespace data
#endif  // SONG_MAPPER_H
