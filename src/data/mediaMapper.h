#ifndef MEDIA_MAPPER_H
#define MEDIA_MAPPER_H


#include "dbMapper.h"
#include <data/media.h>

namespace data {

    class MediaMapper : public DbMapper {
    public:
        MediaMapper(const std::string& connectionString);

        bool get(Media& dest, const std::string& id);
        std::string insert(const std::string& userId, const Media& media);
        bool disable(const std::string& id);
    };


}   // namespace data
#endif  // MEDIA_MAPPER_H
