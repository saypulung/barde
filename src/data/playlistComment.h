#ifndef DATA_PLAYLIST_COMMENT_H
#define DATA_PLAYLIST_COMMENT_H

#include <cppcms/view.h>

#include <data/user.h>

#include <vector>

namespace data {

	struct PlaylistComment : public cppcms::base_content {
        std::string id;
        std::string parentId;
		std::string comment;
        User author;
        time_t createdAt;

        std::vector<PlaylistComment> replies;
	};

}   // namespace data
#endif  // DATA_PLAYLIST_COMMENT_H
