#ifndef DATA_ARTIST_H
#define DATA_ARTIST_H

#include <cppcms/view.h>
#include <string>

namespace data {

    struct Artist : public cppcms::base_content {
        std::string id;
        std::string name;

        Artist() : id("") {}

        bool empty() const {
            return id.empty();
        }
    };

}   // namespace data


#endif  // DATA_ARTIST_H;
