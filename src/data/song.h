#ifndef DATA_SONG_H
#define DATA_SONG_H

#include <cppcms/view.h>

#include <data/artist.h>
#include <data/media.h>
#include <data/playlist.h>
#include <data/songVote.h>

namespace data {

	struct Song : public cppcms::base_content {
        std::string id;
		std::string title;
		Artist artist;
        Media media;
		std::string url;
        unsigned int duration;
        unsigned short position;
        bool showVideo;
		std::string proposer;
        PlaylistItem playlist;
        SongVote vote;

        Song() : showVideo(false), proposer(""), position(0)  {}

        std::string toString() const {
            std::ostringstream oss;
            oss << "{ id: " << id << ", title: " << title << ", artist: " << artist.name
                << ", playlist: " << playlist.id << " }";
            return oss.str();
        }
	};

}   // namespace data
#endif  // DATA_SONG_H
