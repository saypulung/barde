#include "song.h"

#include <cppcms/url_dispatcher.h>
#include <cppcms/json.h>
#include <booster/log.h>

#include <app/playlist.h>
#include <app/validator/songValidator.h>
#include <app/util/stringMethods.h>

#include <data/artistMapper.h>
#include <data/mediaMapper.h>
#include <data/playlistMapper.h>
#include <data/songMapper.h>

#include <sstream>


using namespace cppcms::http;

namespace app {
    Song::Song(cppcms::service& s) :
        app::Master(s)
    {
        dispatcher().assign("/edit/(.+)", &Song::displayEdit, this, 1);
        mapper().assign("/{1}");

        dispatcher().assign("/proposed", &Song::displayProposed, this);
        mapper().assign("");

        dispatcher().map("/ajax-new", &Song::ajaxNew, this);

        dispatcher().map("/ajax-set-playlist/([a-f0-9-]+)/([a-f0-9-]+)", &Song::ajaxSetPlaylist, this, 1, 2);

        dispatcher().map("/ajax-set-duration/([a-f0-9-]+)/(\\d+)", &Song::ajaxSetDuration, this, 1, 2);
    }

    void Song::displayProposed() {
        BOOSTER_DEBUG(__func__);

        data::SongAdminPage pageSong(page_);

        if (! checkAuth(pageSong.user, data::User::ADMINISTRATOR)) {
            forbidAccess(pageSong.user);
            return;
        }

        pageSong.pageTitle = "Proposed songs";

        data::SongMapper songMapper(connectionString_);
        data::PlaylistMapper playlistMapper(connectionString_);

        songMapper.loadPendingSongs(pageSong);
        playlistMapper.loadComingPlaylists(pageSong);

        render("proposedSongs", pageSong);
    }

    void Song::displayEdit(std::string songId) {
        BOOSTER_DEBUG(__func__);

        data::EditSongPage pageSong(page_);

        if (! checkAuth(pageSong.user, data::User::ADMINISTRATOR)) {
            forbidAccess(pageSong.user);
            return;
        }

        data::SongMapper songMapper(connectionString_);

        if (request().request_method() == "POST") {
            pageSong.input.load(context());
            if(! pageSong.input.validate()) {
                pageSong.alerts.errors.push_back("Invalid or missing fields!");
            } else {
                data::Song song;
                song.id = songId;
                song.title = pageSong.input.title.value();
                song.artist.name = pageSong.input.artist.value();
                song.media.id = pageSong.input.fileId.value();

                file* songFile = 0;
                std::string songPath;
                if (pageSong.input.newFile.set()) {
                    songFile = pageSong.input.newFile.value().get();
                    songPath = toUploadRelativePath(songFile, song.artist.name + '-' + song.title);
                } else {
                    songPath = pageSong.input.file.value();
                }

                song.url = pageSong.input.url.value();
                song.showVideo = pageSong.input.showVideo.value();
                song.position = pageSong.input.position.value();

                std::ostringstream msg;
                try {
                    bool newMedia = false;
                    if (songFile) {
                        std::string songFullPath = toFullPath(songPath);
                        songFile->save_to(songFullPath);

                        song.media.type = data::Media::TYPE_SONG;
                        song.media.label = song.title;
                        song.media.file = songPath;
                        song.media.contentType = songFile->mime();
                        song.media.length = songFile->size();
                        newMedia = true;
                        BOOSTER_INFO(__func__) << "Uploaded file " << songFullPath;
                    }

                    if (! update(song, pageSong.user.id, newMedia)) {
                        msg << "Could not modifiy \"" << song.title << "\".";
                        pageSong.alerts.errors.push_back(msg.str());
                        BOOSTER_ERROR(__func__) << msg.str();
                    } else {
                        msg << "Successfully modified \"" << song.title << "\".";
                        pageSong.alerts.success.push_back(msg.str());
                        BOOSTER_INFO(__func__) << msg.str();
                    }
                } catch(const cppcms::cppcms_error& e) {
                    msg << "Could not upload file " << songPath;
                    pageSong.alerts.errors.push_back(msg.str());
                    BOOSTER_ERROR(__func__) << msg.str() << " - - " << e.trace();
                }
            }
        } else if (!songId.empty() && songMapper.loadSong(pageSong.song, songId)) {
            BOOSTER_DEBUG(__func__) << "Loading fields for song " << songId;
            pageSong.input.artist.value(pageSong.song.artist.name);
            pageSong.input.title.value(pageSong.song.title);
            pageSong.input.fileId.value(pageSong.song.media.id);
            pageSong.input.file.value(pageSong.song.media.file);
            pageSong.input.url.value(pageSong.song.url);
            pageSong.input.showVideo.value(pageSong.song.showVideo);
            pageSong.input.position.value(pageSong.song.position);
        }

        pageSong.pageTitle = "Modify a song";

        render("editSong", pageSong);
    }

    void Song::ajaxNew() {
        data::User user;

        if (! checkAuth(user, data::User::CITIZEN)) {
            response().make_error_response(response::forbidden);
            BOOSTER_WARNING(__func__) << "Forbid user "
                << user.alias << " to add new song";
            return;
        }

        BOOSTER_DEBUG(__func__);

        data::Song song;
        song.title = request().post("title");
        song.artist.name = request().post("artist");
        song.url = request().post("url");

        validator::SongValidator validator(song);
        bool success = validator.validate();
        std::string message = validator.lastMessage();

        if (success) {
            success = insert(user, song);
            std::ostringstream oss;
            if (success) {
                oss << "Successfully proposed \"" << song.title << "\". Thanks for your contribution to the playlist ^_^";
            } else {
                oss << "Could not submit the song, please retry later!";
            }
            message = oss.str();
        }

        if (success) {
            BOOSTER_INFO(__func__) << message;
        } else {
            BOOSTER_ERROR(__func__) << message;
        }

        cppcms::json::value jsonOutput;
        jsonOutput["success"] = success;
        jsonOutput["message"] = message;
        response().out() << jsonOutput;
    }

    void Song::ajaxSetPlaylist(const std::string& songId, const std::string& playlistId) {
        data::User user;

        if (! checkAuth(user, data::User::ADMINISTRATOR)) {
            response().make_error_response(response::forbidden);
            BOOSTER_WARNING(__func__) << "Forbid user "
                << user.alias << " to set song playlist";
            return;
        }

        BOOSTER_DEBUG(__func__);

        data::SongMapper songMapper(connectionString_);

        bool result = songMapper.updatePlaylistId(songId, playlistId);

        cache().rise(user.id);
        BOOSTER_DEBUG(__func__) << "Clean caches for user ID " << user.id;

        cppcms::json::value jsonOutput;
        jsonOutput["success"] = result;
        response().out() << jsonOutput;
    }

    void Song::ajaxSetDuration(const std::string& songId, std::string duration) {
        data::User user;

        if (! checkAuth(user, data::User::GUEST)) {
            response().make_error_response(response::forbidden);
            BOOSTER_WARNING(__func__) << "Forbid user "
                << user.alias << " to set song playlist";
            return;
        }

        BOOSTER_DEBUG(__func__);

        data::SongMapper songMapper(connectionString_);

        bool result = songMapper.updateDuration(songId, std::stoi(duration));

        cppcms::json::value jsonOutput;
        jsonOutput["success"] = result;
        response().out() << jsonOutput;
    }

    bool Song::insert(const data::User& proposer, const data::Song& song) {
        bool success = false;

        data::ArtistMapper artistMapper(connectionString_);
        data::SongMapper songMapper(connectionString_);

        data::Song song2 = song;    // modifiable Song
        if (!artistMapper.getByName(song2.artist, song.artist.name)) {
            song2.artist.id = artistMapper.insert(song2.artist);
        }

        if (song2.artist.empty()) {
            BOOSTER_ERROR(__func__) << "Could not create artist " << song.artist.name;
        } else {
            success = songMapper.insert(proposer, song2);
        }

        return success;
    }

    bool Song::update(const data::Song& song, const std::string& userId, bool newMedia) {
        data::Song song2 = song;    // modifiable Song
        song2.artist.id = loadSongArtistId(song.id);
        if (song2.artist.empty()) {
            BOOSTER_ERROR(__func__) << "Missing artist for song " << song.id;
            return false;
        }

        data::ArtistMapper artistMapper(connectionString_);
        data::MediaMapper mediaMapper(connectionString_);
        data::SongMapper songMapper(connectionString_);

        if (newMedia) {
            mediaMapper.disable(song.media.id);
            song2.media.id = mediaMapper.insert(userId, song.media);
        }
        artistMapper.update(song2.artist);
        songMapper.update(song2);
        return true;
    }

    std::string Song::loadSongArtistId(const std::string& songId) {
        data::Song song;
        data::SongMapper songMapper(connectionString_);

        if (songMapper.loadSong(song, songId)) {
            return song.artist.id;
        }
        return 0;
    }

}   // namespace app

